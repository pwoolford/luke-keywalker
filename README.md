# Luke-Keywalker

## Introduction
This project is a clone of the utility [`kwp`](https://github.com/hashcat/kwprocessor), with some tweaks but also some missing features.

I made this because I found kwp to be slow for what it did, but mostly because kwp produces result I would consider incorrect (e.g. wrapping aroud from one edge to an opposite edge).

## Maintenance
This was a fun little project, and I'm not indending to continue development and implement new features at this time.

I will, however, fix any bugs reported. If you decide to use this tool, let me know! :)

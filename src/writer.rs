use std::io::{stdout, Write};
use std::fs::File;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread::{spawn, JoinHandle};
use std::path::PathBuf;

pub struct WriterThread {
    _thread: JoinHandle<()>,
    sender: Sender<String>,
}

impl WriterThread {
    pub fn new(output_file: Option<PathBuf>) -> WriterThread {
        let (tx, rx): (Sender<String>, Receiver<String>) = channel();

        WriterThread {
            _thread: spawn(move || {
                match output_file {
                    Some(f) => {
                        let mut out_target = File::open(f).unwrap();
                        while let Ok(s) = rx.recv() {
                            match writeln!(out_target,"{}", s) {
                                Ok(()) => continue,
                                Err(_) => break,
                            }
                        }
                    },
                    None => {
                        let stdout = stdout();
                        let mut out_target = stdout.lock();

                        while let Ok(s) = rx.recv() {
                            match writeln!(out_target,"{}", s) {
                                Ok(()) => continue,
                                Err(_) => break,
                            }
                        }
                    }
                }

            }),
            sender: tx,
        }
    }

    pub fn sender(&self) -> &Sender<String> {
        &self.sender
    }
}

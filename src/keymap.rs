use crate::directions::Direction;
use crate::keymap::Layer::{AltGr, Basic, Shift};
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::default::Default;
use std::io::{BufRead, BufReader, Read};
use std::iter::Iterator;

pub static LAYERS: [Layer; 3] = [Basic, Shift, AltGr];

/// A Coordinate in a key map.
/// Stores the Layer, Column, and Row of the referenced key
#[derive(PartialOrd, PartialEq, Eq, Hash, Clone, Copy)]
pub struct Coord(Layer, isize, isize);

impl std::ops::AddAssign<Direction> for Coord {
    fn add_assign(&mut self, other: Direction) {
        let (add_x, add_y) = other.into();
        self.1 += add_x;
        self.2 += add_y;
    }
}

impl Into<Coord> for (Layer, isize, isize) {
    fn into(self) -> Coord {
        Coord(self.0, self.1, self.2)
    }
}

/// Keyboard layers
#[derive(PartialOrd, PartialEq, Eq, Hash, Clone, Copy, Debug)]
pub enum Layer {
    /// Keyboard with no modifiers
    Basic,
    /// Keyboard with Shift held
    Shift,
    /// Keyboard with AltGr held
    AltGr,
}

/// Error type for usize -> Layer conversion
#[derive(Debug)]
pub struct LayerRangeError(usize);

impl TryFrom<usize> for Layer {
    type Error = LayerRangeError;
    fn try_from(val: usize) -> Result<Layer, LayerRangeError> {
        match val {
            0 => Ok(Basic),
            1 => Ok(Shift),
            2 => Ok(AltGr),
            _ => Err(LayerRangeError(val)),
        }
    }
}

/// map coordinates to characters pulled from keymap files.
#[derive(Default)]
pub struct KeyMap(pub HashMap<Coord, char>);

impl KeyMap {
    pub fn find(&self, c: char) -> Option<Coord> {
        let coords: Vec<(&Coord, &char)> = self.0.iter().filter(|e| *e.1 == c).collect();
        if !coords.is_empty() {
            Some(*coords[0].0)
        } else {
            None
        }
    }

    pub fn path(
        &self,
        layer: Layer,
        mut origin: Coord,
        direction: Direction,
        distance: usize,
    ) -> Option<(String, Coord)> {
        let mut path = String::new();
        origin.0 = layer;

        for _ in 0..distance {
            origin += direction;
            if let Some(&entry) = self.0.get(&origin) {
                path.push(entry)
            } else {
                return None; // route found an empty key before end
            }
        }

        Some((path, origin))
    }
}

/// Error type for parsing a MapFile
#[derive(Debug)]
pub enum KeyMapParseError {
    Io(std::io::Error),
    NotEnoughLines
}

impl From<std::io::Error> for KeyMapParseError {
    fn from(error: std::io::Error) -> KeyMapParseError {
        KeyMapParseError::Io(error)
    }
}

/// Parse a keymap from a keymap file.
pub fn parse_keymap<T: Read>(reader: BufReader<T>) -> Result<KeyMap, KeyMapParseError> {
    let mut map = KeyMap::default();

    for (line_num, line) in reader.lines().enumerate() {
        let layer = (line_num / 4).try_into().unwrap();
        let keyboard_line = line_num % 4;

        for (column, key) in line?.chars().enumerate() {
            map.0
                .insert(Coord(layer, column as isize, keyboard_line as isize), key);
        }

        if line_num == 11 {
            return Ok(map); // we're done
        }
    }

    Err(KeyMapParseError::NotEnoughLines)
}

use std::convert::TryFrom;
use std::mem::transmute;
use Direction::*;

// :( probs should make it safe instead

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
pub enum Direction {
    SouthWest = 1,
    South,
    SouthEast,
    West,
    Stay,
    East,
    NorthWest,
    North,
    NorthEast,
}

pub static DIRECTIONS: [Direction; 9] = [
    SouthWest, South, SouthEast, West, Stay, East, NorthWest, North, NorthEast,
];

pub struct RangeError(u32);

impl TryFrom<u8> for Direction {
    type Error = RangeError;
    fn try_from(val: u8) -> Result<Direction, Self::Error> {
        if 0 < val && val < 10 {
            unsafe {
                Ok(transmute(val))
            }
        } else {
            Err(RangeError(u32::from(val)))
        }
    }
}

impl TryFrom<char> for Direction {
    type Error = RangeError;
    fn try_from(val: char) -> Result<Direction, Self::Error> {
        let str_val = val.to_string();
        match u8::from_str_radix(str_val.as_ref(), 16) {
            Ok(byte) =>
            Self::try_from(byte),
        Err(_) => Err(RangeError(u32::from(val)))
        }
    }
}

impl Into<(isize, isize)> for Direction {
    fn into(self) -> (isize, isize) {
        match self {
            SouthWest => (-1, -1),
            South => (0, -1),
            SouthEast => (1, -1),
            West => (-1, 0),
            Stay => (0, 0),
            East => (1, 0),
            NorthWest => (-1, 1),
            North => (0, 1),
            NorthEast => (1, 1),
        }
    }
}

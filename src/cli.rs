#![cfg_attr(feature = "cargo-clippy", allow(clippy::single_match))]

use crate::directions::{Direction, DIRECTIONS};
use crate::keymap::{Layer, LAYERS};
use std::path::{PathBuf};

use structopt::StructOpt;

static BOOL_VALUES: [&'static str; 2] = ["true", "false"];

#[derive(Debug)]
pub struct Opts {
    pub directions: Vec<Direction>,
    pub layers: Vec<Layer>,
    pub output: Option<PathBuf>,
    pub basechars_file: PathBuf,
    pub keymap_file: PathBuf,
    pub routes_file: PathBuf,
}

#[derive(StructOpt, Debug)]
struct RawOpts {
    #[structopt(
        short = "b",
        long = "keyboard-basic",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of basic layer"
    )]
    pub basic_layer: Option<bool>,
    #[structopt(
        short = "s",
        long = "keyboard-shift",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of shift layer"
    )]
    pub shift_layer: Option<bool>,
    #[structopt(
        short = "a",
        long = "keyboard-altgr",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of altgr layer"
    )]
    pub altgr_layer: Option<bool>,
    #[structopt(
        short = "z",
        long = "keyboard-all",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of all layers"
    )]
    pub all_layers: Option<bool>,
    #[structopt(
        short = "1",
        long = "keywalk-southwest",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of southwest direction"
    )]
    pub southwest_dir: Option<bool>,
    #[structopt(
        short = "2",
        long = "keywalk-south",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of south direction"
    )]
    pub south_dir: Option<bool>,
    #[structopt(
        short = "3",
        long = "keywalk-southeast",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of southeast direction"
    )]
    pub southeast_dir: Option<bool>,
    #[structopt(
        short = "4",
        long = "keywalk-west",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of west direction"
    )]
    pub west_dir: Option<bool>,
    #[structopt(
        short = "5",
        long = "keywalk-repeat",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of repeated keys"
    )]
    pub stay_dir: Option<bool>,
    #[structopt(
        short = "6",
        long = "keywalk-east",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of east direction"
    )]
    pub east_dir: Option<bool>,
    #[structopt(
        short = "7",
        long = "keywalk-northwest",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of northwest direction"
    )]
    pub northwest_dir: Option<bool>,
    #[structopt(
        short = "8",
        long = "keywalk-north",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of north direction"
    )]
    pub north_dir: Option<bool>,
    #[structopt(
        short = "9",
        long = "keywalk-snortheast",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of southwest direction"
    )]
    pub northeast_dir: Option<bool>,
    #[structopt(
        short = "0",
        long = "keywalk-all",
        raw(possible_values = "&BOOL_VALUES"),
        help = "Allow use of all directions"
    )]
    pub all_dirs: Option<bool>,
    #[structopt(short = "o", long = "output-file", parse(from_os_str))]
    pub output_file: Option<PathBuf>,
    #[structopt(parse(from_os_str))]
    pub basechars_file: PathBuf,
    #[structopt(parse(from_os_str))]
    pub keymap_file: PathBuf,
    #[structopt(parse(from_os_str))]
    pub routes_file: PathBuf,
}

pub fn get_opts() -> Opts {
    let cli = RawOpts::from_args();

    let mut allowed_directions = Vec::new();
    if let Some(true) = cli.all_dirs {
        allowed_directions.extend_from_slice(&DIRECTIONS);
    } else {
        match cli.southwest_dir {
            Some(true) => allowed_directions.push(Direction::SouthWest),
            _ => {}
        }

        match cli.south_dir {
            Some(false) => {}
            _ => allowed_directions.push(Direction::South),
        }

        match cli.southeast_dir {
            Some(true) => allowed_directions.push(Direction::SouthEast),
            _ => {}
        }

        match cli.west_dir {
            Some(false) => {}
            _ => allowed_directions.push(Direction::West),
        }

        match cli.stay_dir {
            Some(true) => allowed_directions.push(Direction::Stay),
            _ => {}
        }

        match cli.east_dir {
            Some(false) => {}
            _ => allowed_directions.push(Direction::East),
        }

        match cli.northwest_dir {
            Some(true) => allowed_directions.push(Direction::NorthWest),
            _ => {}
        }

        match cli.north_dir {
            Some(false) => {}
            _ => allowed_directions.push(Direction::North),
        }

        match cli.northeast_dir {
            Some(true) => allowed_directions.push(Direction::NorthEast),
            _ => {}
        }
    }

    let mut allowed_layers = Vec::new();
    if let Some(true) = cli.all_layers {
        allowed_layers.extend_from_slice(&LAYERS);
    } else {
        match cli.basic_layer {
            Some(false) => {}
            _ => allowed_layers.push(Layer::Basic),
        }

        match cli.shift_layer {
            Some(true) => allowed_layers.push(Layer::Shift),
            _ => {}
        }

        match cli.altgr_layer {
            Some(true) => allowed_layers.push(Layer::AltGr),
            _ => {}
        }
    }

    Opts {
        directions: allowed_directions,
        layers: allowed_layers,
        output: cli.output_file,
        basechars_file: cli.basechars_file,
        keymap_file: cli.keymap_file,
        routes_file: cli.routes_file,
    }
}

use std::convert::From;
use std::io::{BufRead, BufReader, Read};

/// A route is a Vec of usizes, where the values represent the number of keys in a run before
/// changing either a) the direction, or b) the layer (basic/shift/altgr)
pub type Route = Vec<usize>;

#[derive(Debug)]
pub enum RouteParseError {
    Io(std::io::Error),
    InvalidChar(usize, usize, char), // line, position, value
}

impl From<std::io::Error> for RouteParseError {
    fn from(e: std::io::Error) -> RouteParseError {
        RouteParseError::Io(e)
    }
}

pub fn parse_routes<T: Read>(reader: BufReader<T>) -> Result<Vec<Route>, RouteParseError> {
    let mut out = Vec::new();

    for (line_num, line) in reader.lines().enumerate() {
        let mut route = Route::new();
        for (pos, val) in line?.chars().enumerate() {
            match val.to_digit(16) {
                Some(d) => {
                    route.push(d as usize);
                }
                None => return Err(RouteParseError::InvalidChar(line_num, pos, val)),
            }
        }
        out.push(route);
    }

    Ok(out)
}


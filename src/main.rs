// TODO: multi-thread enable
// for now, single thread only
/*
// A global flag to shutdown the program gracefully.
// Since it will be false for basically the whole program life,
// the branch predictor should eliminate most of the performance impact
use std::sync::{Arc,atomic::AtomicBool};
type ShutdownFlag = Arc<AtomicBool>;

// run workers on raw threads.
use std::thread;
*/

use std::fs::File;
use std::io::{BufReader};
use std::sync::mpsc::Sender;


use rayon::prelude::*;
use rayon::iter::IntoParallelIterator;

mod cli;
mod constants;
mod directions;
mod keymap;
mod keywalk;
mod route;
mod writer;

use keymap::{parse_keymap, KeyMap};

fn main() {
    let opts = cli::get_opts();

    let allowed_directions = opts.directions.as_slice();
    let allowed_layers = opts.layers.as_slice();

    let outwriter = writer::WriterThread::new(opts.output);
    let send_channel = outwriter.sender();

    let base: String = match std::fs::read_to_string(opts.basechars_file) {
        Ok(s) => s,
        Err(e) => panic!("Error opening keymap file: {}", e)
    };

    let keymap_file = match File::open(opts.keymap_file) {
        Ok(f) => f,
        Err(e) => panic!("Error opening keymap file: {}", e),
    };

    let keymap: KeyMap = parse_keymap(BufReader::new(keymap_file)).unwrap();

    let route_file = match File::open(opts.routes_file) {
        Ok(f) => f,
        Err(e) => panic!("Error opening route file: {}", e),
    };
    let routes = match route::parse_routes(BufReader::new(route_file)) {
        Ok(r) => r,
        Err(route::RouteParseError::Io(e)) => panic!("Error reading route file: {}", e),
        Err(route::RouteParseError::InvalidChar(line, pos, chr)) => panic!(
            "Error reading route file: invalid character \"{}\" (line: {}, pos: {})",
            chr, line, pos
        ),
    };

    base.chars()
        .filter(|c| keymap.find(*c).is_some())// only process if base_char is in the keymap
        .map(|c| (c, send_channel.clone())) //clone a sender for each thread
        .collect::<Vec<(char, Sender<String>)>>() // maps are lazy, collect to evalute the map
        .into_par_iter()
        .map(|(base_char, writer): (char, Sender<String>)| {
            for route in &routes {
                let _ = keywalk::KeyWalk::new(base_char, &keymap, &*route, allowed_directions, allowed_layers)
                    .map(|s:String| {
                        writer.send(s)
                    })
                    .collect::<Result<(), std::sync::mpsc::SendError<String>>>();
            }
        })
        .collect::<()>();
}

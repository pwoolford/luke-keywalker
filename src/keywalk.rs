use crate::directions::Direction;
use crate::keymap::{Coord, KeyMap, Layer};

pub struct KeyWalk<'kw> {
    path: Vec<Coord>,
    fragments: Vec<String>,
    keymap: &'kw KeyMap,
    route: &'kw [usize],
    route_index: usize,
    directions: &'kw [Direction],
    direction_indexes: Vec<usize>,
    layers: &'kw [Layer],
    layer_indexes: Vec<usize>,
}

impl<'kw> KeyWalk<'kw> {
    pub fn new(
        start: char,
        keymap: &'kw KeyMap,
        route: &'kw [usize],
        directions: &'kw [Direction],
        layers: &'kw [Layer],
    ) -> KeyWalk<'kw> {
        KeyWalk {
            path: vec![keymap.find(start).unwrap()],
            fragments: vec![start.to_string()],
            keymap,
            route,
            route_index: 0,
            directions,
            direction_indexes: vec![0],
            layers,
            layer_indexes: vec![0],
        }
    }

    pub(self) fn descend(&mut self) -> Option<String> {
        'descend: loop {
            if self.path.is_empty() {
                break 'descend; // only happens when we have ascended at the end of the iterator
            }
            for direction_index in *self.direction_indexes.last().unwrap()..self.directions.len() {
                *self.direction_indexes.last_mut().unwrap() = direction_index; //keep it up to date
                for layer_index in *self.layer_indexes.last().unwrap()..self.layers.len() {
                    *self.layer_indexes.last_mut().unwrap() = layer_index; // keep it up to date
                    if let Some((walk_string, new_location)) = self.keymap.path(
                        self.layers[layer_index],
                        *self.path.last().unwrap(),
                        self.directions[direction_index],
                        self.route[self.route_index],
                    ) {
                        //update state
                        self.fragments.push(walk_string);
                        self.path.push(new_location);
                        self.route_index +=1;
                        *self.direction_indexes.last_mut().unwrap() += 1; // prevent infinite loop
                        *self.layer_indexes.last_mut().unwrap() +=1; //prevent infinite loop

                        //check if we're at the end of the route
                        if self.route_index == self.route.len() {
                            //time to return
                            let return_string = self.fragments.join(""); //get the string to return
                            self.route_index -= 1; // undo the route_descent
                            let _  = self.path.pop(); //discard location we just pushed
                            let _ = self.fragments.pop(); // discard the string fragment we just pushed

                            return Some(return_string)
                        }

                        //need to descend
                        self.direction_indexes.push(0); //new end index
                        self.layer_indexes.push(0); //new end index
                        continue 'descend; //start again one layer deeper
                    }
                }
            }

            // we've looped through all the possible descent paths, go back up
            let _ = self.fragments.pop();
            let _ = self.path.pop();
            self.route_index.wrapping_sub(1);
            let _ = self.direction_indexes.pop();
            let _ = self.layer_indexes.pop();

        }

        // we're at the end of the iterator, return None
        None
    }
}

impl<'kw> Iterator for KeyWalk<'kw> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        self.descend()
    }
}
